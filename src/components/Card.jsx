import React from 'react'
import styles from './card.module.css'

export default function Card (props) {
  const { name, min, max, img, onClose } = props //Destructiring

  // acá va tu código
  return (
    <div className={styles.card}>
      <button className={styles.card__button} onClick={onClose}>
        X
      </button>
      <h5 className={styles.card__div}>Card{props.name}</h5>
      <div>
        <p className={styles.card__p}>
          Min: <br /> {min}
        </p>
        <p className={styles.card__p}>
          Max: <br /> {max}
        </p>
        <img
          className={styles.card__img}
          src={`http://openweathermap.org/img/wn/${img}@2x.png`}
          alt={name}
        />
      </div>
    </div>
  )
}
