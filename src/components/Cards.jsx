import React from 'react'
import Card from './Card'
import { Container, Button } from './styleCards'

export default function Cards (props) {
  const { cities } = props
  // acá va tu código
  // tip, podés usar un map
  return (
    <Container>
      <Button>STYLED</Button>
      {cities &&
        cities.map((city, index) => (
          <Card
            key={index}
            max={city.main.temp_max}
            min={city.main.temp_min}
            name={city.name}
            img={city.weather[0].icon}
            onClose={() => alert(city.name)}
          />
        ))}
    </Container>
  )
}
