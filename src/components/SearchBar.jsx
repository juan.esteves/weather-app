import React, { useState } from 'react'

export default function SearchBar (props) {
  const [city, setCity] = useState('')

  return (
    <form
      onSubmit={event => {
        event.preventDefault()
        console.log(city)
      }}
    >
      <input
        type='text'
        placeholder='Ciudad...'
        value={city}
        onChange={event => setCity(event.target.value)}
      />

      <input type='submit' value='Add' />
    </form>
  )
}
